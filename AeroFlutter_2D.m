%***********************************************************************************************
% Two-dimensional flutter analysis
% Developed by Dr. Bruno Roccia
%       Bergen Offshore Wind Centre, University of Bergen, Norway (2024)
% Contact: bruno.roccia@uib.no
%***********************************************************************************************

clear all
close all
clc

% This Matlab script numerically integrates in the time domain the
% governing equations of a two-dimensional aeroelastic system. To
% integrate the system of ODEs, the script uses the integrator ode45

% Output variable: structure RESPONSE containing the following fields
%                  .Time (vector containing the time)
%                  .Plunge (vector containing plunge time-series solution evaluated at Time)
%                  .Pitching (vector containing pitching time-series solution evaluated at Time)
%                  .DPlunge (vector containing plunge velocity solution evaluated at Time)
%                  .DPitching (vector containing pitching angular velocity solution evaluated at Time)

%% DATA

DATA.C      = 18.288;               % Wing chord  
DATA.kh     = 9.9818e+3;            % Linear plunge spring stiffness
DATA.kt     = 1.614834e+6;          % Linear pitching spring stiffness
DATA.m      = 12879.8;              % Mass per unit length
DATA.Ip     = 6.7005e+5;            % Wing mss moment of inertia
DATA.dh     = 0;                    % Plunge damping coefficient
DATA.dt     = 0;                    % Pitching damping coefficient
DATA.khc    = 0;                    % Cubic plunge spring stiffness
DATA.ktc    = 1.5 * DATA.kt;        % Cubic pitching spring stiffness
DATA.xs     = 1.0;                  % Elastic axis dimensionless distance from the leading edge
DATA.xa     = 0;                    % Distance between the elastic axis and mass center
DATA.U      = 60;                 % Free-stream velocity
DATA.Rhoinf = 1.225;                % Air density

NUM.DT      = 0.01;                 % Time step
NUM.TFinal  = 10000;                % simulation time
NUM.CI      = [0 1*pi/180 0 0];     % Initial conditions
% [14,30,45,47,48,48.5,48.8,49,52,58,65,75,85];

%% Dimensionless quantities

Wh = sqrt(DATA.kh/DATA.m);
Wt = sqrt(DATA.kt/DATA.Ip);

DATA.Xi_h   = DATA.dh / (2*sqrt(DATA.m*DATA.kh));          
DATA.Xi_a   = DATA.dt / (2*sqrt(DATA.m*DATA.kt));            
DATA.mu     = DATA.m / (pi*DATA.Rhoinf*(DATA.C/2)^2);         
DATA.ra     = sqrt(DATA.Ip/(DATA.m*(DATA.C/2)^2));        
DATA.ah     = DATA.xs - 1;        
DATA.xa     = DATA.xa;         
DATA.Omb    = Wh/Wt;         
DATA.Beta   = DATA.ktc / DATA.kt;           
DATA.Gamma  = DATA.khc / DATA.kh;         
DATA.Uinf   = DATA.U / (DATA.C/2*Wt);      

DATA.Psi1   = 0.165;
DATA.Psi2   = 0.335;
DATA.Eps1   = 0.0455;
DATA.Eps2   = 0.3;

%% Solution procedure

Tspan = [0 NUM.TFinal];
CInitial = [NUM.CI, 0, 0, 0, 0];

Term = Matrices (DATA);
[Tout, Zout] = ode45 (@Func, Tspan, CInitial, [], DATA, Term, NUM);
SOL.Time = Tout;
SOL.Z    = Zout;

RESPONSE.Time = SOL.Time * DATA.C/2 / DATA.U;
RESPONSE.Plunge = SOL.Z(:,1) * DATA.C/2;
RESPONSE.Pitching = SOL.Z(:,2) * 180 / pi;
RESPONSE.DPlunge = SOL.Z(:,3) * (DATA.C/2)^2 / DATA.U;
RESPONSE.DPitching = SOL.Z(:,4) * 180 / pi * DATA.C/2 / DATA.U;

%% PLOT

figure (1)
plot (RESPONSE.Plunge, RESPONSE.DPlunge,"Color",'b')
hold on
plot(xlim,[0 0], 'color','k','linestyle','--')
title('Plunge life cycle oscillation(LCO)')
ylabel('Plunge velocity [m/s]')
xlabel('Plunge amplitude [m]')

figure(2)
plot (RESPONSE.Pitching, RESPONSE.DPitching,"Color",'b')
hold on
plot(xlim,[0 0], 'color','k','linestyle','--')
title('Pitching life cycle oscillation(LCO)')
ylabel('Piching velocity [m/s]')
xlabel('Pitching amplitude [º]')

%% Functions
flutter_u=48.9;
Pitch_u=[14,30,45,47,48,48.5,48.8,48.9,49,52,58,65,75,85];
Pitch_a=[0,0,0,0,0,0,0,0.45,1.85,16,28.7,39,52,64];
figure(3)
plot(Pitch_u/flutter_u,Pitch_a)
hold on
plot(Pitch_u/flutter_u,Pitch_a,'*')
title('Pitch bifurcation curve')
xlabel('Dimensionless free-stream velocity U/U_f')
ylabel('Pitch- LCO amplitude [º]')
grid on
Plunge_u=[14,30,45,47,48,48.5,48.8,48.9,49,52,58,65,75,85];
Plunge_a=[0,0,0,0,0,0,0,0.14,0.38,3.42,6.11,8.6,11.7,14.5];
figure(4)
plot(Plunge_u/flutter_u,Plunge_a)
hold on
plot(Plunge_u/flutter_u,Plunge_a,'*')
title('Plunge bifurcation curve')
xlabel('Dimensionless free-stream velocity U/U_f')
ylabel('Plunge- LCO amplitude [m]')
grid on

