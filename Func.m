function DZ = Func(t, Z, DATA, Term, NUM)

% This function evaluates the the governing system of ODEs
% Inputs: data strucutre variables DATA, Term and NUM, time t, and solution Z(t)
% DATA: structure variable containing all the parameters of the problem under study
% NUM : structure variable containing parameters associated to numerical inegration
% Term: structure variable containing auxiliar matrices
% t   : real variable (current time)
% Z   : current state of the system. Array of dimension 8x1
% Outputs: Derivative of Z with respecto time. Array of dimension 8x1

ra    = DATA.ra;
xa    = DATA.xa;
ah    = DATA.ah;
mu    = DATA.mu;
Beta  = DATA.Beta;   
Gamma = DATA.Gamma;  
Omb   = DATA.Omb;
Uinf  = DATA.Uinf;

Psi1  = DATA.Psi1;  
Psi2  = DATA.Psi2;  
Eps1  = DATA.Eps1; 
Eps2  = DATA.Eps2;

DPhi = Psi1*Eps1*exp(-Eps1*t) + Psi2*Eps2*exp(-Eps2*t);

B0 = 0.5 - ah;
B1 = 0.5 + ah;

Aa = zeros(2,2);
Aa(1,1) = -2/mu*DPhi;
Aa(1,2) = -2/mu*B0*DPhi;
Aa(2,1) = 2*B1/(mu*ra^2)*DPhi;
Aa(2,2) = 2*B1/(mu*ra^2)*B0*DPhi;

Q0 = zeros(2,1);
Q0(1,1) = NUM.CI(1);
Q0(2,1) = NUM.CI(2);

Ms = [1 xa*cos(Z(2,1)); xa/ra^2*cos(Z(2,1)) 1];

Gs = zeros(2,1);
Gs(1,1) = -xa*sin(Z(2,1))*Z(4,1)^2;

Fs = zeros(2,1);
Fs(1,1) = Omb^2/Uinf^2 * (Z(1,1) + Gamma*Z(1,1)^3);
Fs(2,1) = 1/Uinf^2 * (Z(2,1) + Beta*Z(2,1)^3);


%% Solving the linear system for DZ

Dq = [Z(3,1); Z(4,1)];
q  = [Z(1,1); Z(2,1)];
W  = [Z(5,1); Z(6,1); Z(7,1); Z(8,1)];

DZ = zeros (8,1);

DZ(1,1) = Z(3,1);
DZ(2,1) = Z(4,1);

DZ(3:4,1) = (Ms + Term.Ma) \ (-Term.Cs*Dq - Fs - Gs - Term.Ca*Dq - Term.Ka*q - Aa*Q0 - Term.Sa*W);

DZ(5,1) = Z(1,1) - DATA.Eps1*Z(5,1);
DZ(6,1) = Z(1,1) - DATA.Eps2*Z(6,1);
DZ(7,1) = Z(2,1) - DATA.Eps1*Z(7,1);
DZ(8,1) = Z(2,1) - DATA.Eps2*Z(8,1);

end