function Term = Matrices (DATA)

% This function evaluates computes the aerodynamic and structural matrices
% which are independent of the configuration and/or time

% Inputs: data strucutre variables DATA
% DATA: structure variable containing all the parameters of the problem under study
% Outputs: structure variable containing aerodynamic and structural
% matrices

Xi_h  = DATA.Xi_h;        % Ratio C_h / 2(m*K_h)^(1/2)
Xi_a  = DATA.Xi_a;        % Ratio C_a / 2(I_a*K_a)^(1/2)
mu    = DATA.mu;          % Ratio m/(pi*Rho*b^2)
ra    = DATA.ra;          % sqrt (I_a/(m*b^2))
ah    = DATA.ah;          % Position of the elastic axis / b
xa    = DATA.xa;          % Position of the mas center / b
Omb   = DATA.Omb;         % Frequency ratio Omega_h/Omega_a
Beta  = DATA.Beta;        % Cubic coefficient in pitch
Gamma = DATA.Gamma;       % Cubic coefficient in plunge
Uinf  = DATA.Uinf;        % non-dimensional flow velocity U/(b*Omega_a)

Psi1  = DATA.Psi1;  
Psi2  = DATA.Psi2;  
Eps1  = DATA.Eps1; 
Eps2  = DATA.Eps2;

Phi0  = 1 - Psi1 - Psi2;
DPhi0 = Psi1*Eps1 + Psi2*Eps2;

B0 = 0.5 - ah;
B1 = 0.5 + ah;

%% Structural matrices

Cs = zeros(2,2);
Cs(1,1) = 2*Xi_h*Omb/Uinf;
Cs(2,2) = 2*Xi_a/Uinf;

%% Aerodynamic matrices

Ma = zeros(2,2);
Ma(1,1) = 1/mu;
Ma(1,2) =-ah/mu;
Ma(2,1) = -ah/(mu*ra^2);
Ma(2,2) = 1/(mu*ra^2)*(ah^2+1/8);

Ca =zeros(2,2);
Ca(1,1) = 2/mu*Phi0;
Ca(1,2) = 1/mu*(1+2*B0*Phi0);
Ca(2,1) = -2*B1/(mu*ra^2)*Phi0;
Ca(2,2) = B0/(mu*ra^2)*(1-2*B1*Phi0);

Ka =zeros(2,2);
Ka(1,1) = 2/mu*DPhi0;
Ka(1,2) = 2/mu*(Phi0 + B0*DPhi0);
Ka(2,1) = -2*B1/(mu*ra^2)*DPhi0;
Ka(2,2) = -2*B1/(mu*ra^2)*(Phi0 + B0*DPhi0);

Sa = zeros(2,4);
Sa(1,1) = -2/mu*Eps1^2*Psi1;
Sa(1,2) = -2/mu*Eps2^2*Psi2;
Sa(1,3) = 2/mu*(1-B0*Eps1)*Eps1*Psi1;
Sa(1,4) = 2/mu*(1-B0*Eps2)*Eps2*Psi2;
Sa(2,1) = 2*B1/(mu*ra^2)*Eps1^2*Psi1;
Sa(2,2) = 2*B1/(mu*ra^2)*Eps2^2*Psi2;
Sa(2,3) = -2*B1/(mu*ra^2)*(1-B0*Eps1)*Eps1*Psi1;
Sa(2,4) = -2*B1/(mu*ra^2)*(1-B0*Eps2)*Eps2*Psi2;

%%

Term.Cs = Cs;
Term.Ma = Ma;
Term.Ca = Ca;
Term.Ka = Ka;
Term.Sa = Sa;

end